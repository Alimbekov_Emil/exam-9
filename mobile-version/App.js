import React, { useEffect, useState } from "react";
import { StyleSheet, View, SafeAreaView, ScrollView, Text } from "react-native";
import axiosContacts from "./axiosContacts";
import Contact from "./Component/Contact/Contact";

const App = () => {
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axiosContacts.get("contacs.json");
        const contacsResponse = Object.keys(response.data).map((id) => ({
          ...response.data[id],
          id,
        }));
        setContacts(contacsResponse);
      } catch (e) {
        console.error(e);
      }
    };
    fetchData();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Text>My Contact</Text>
      </View>
      <ScrollView>
        {contacts.map((contact) => {
          return (
            <Contact
              key={contact.id}
              name={contact.name}
              photo={contact.photo}
              email={contact.email}
              phone={contact.phone}
              id={contact.id}
            />
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 20,
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "flex-start",
  },
});

export default App;
