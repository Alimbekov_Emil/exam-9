import React, { useState } from "react";
import {
  View,
  Image,
  Text,
  Modal,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import MeModal from "../MeModal/MeModal";

const Contact = (props) => {
  const [modal, setModal] = useState(false);

  const closeModal = () => {
    setModal(false);
  };
  const showModal = () => {
    setModal(true);
  };
  return (
    <>
      {modal === true ? (
        <MeModal
          name={props.name}
          photo={props.photo}
          email={props.email}
          phone={props.phone}
          closeModal={closeModal}
        />
      ) : null}
      <TouchableOpacity onPress={showModal}>
        <View style={styles.contact}>
          <View>
            <Image
              source={{ uri: props.photo }}
              style={{ width: 100, height: 100 }}
            />
          </View>
          <View style={styles.block}>
            <Text>{props.name}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  contact: {
    marginTop: 10,
    marginBottom: 20,
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#000",
    padding: 10,
  },
  block: {
    marginLeft: 70,
  },
});

export default Contact;
