import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Contact from "../../Components/Contact/Contact";
import Spinner from "../../Components/UI/Spinner/Spinner";
import { fetchContacts } from "../../store/actions/contactsActions";
import "./ContactsList.css";

const ContactsList = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchContacts());
  }, [dispatch]);

  const contacts = useSelector((state) => state.contacts.contacts);
  const loading = useSelector((state) => state.contacts.loading);

  return (
    <div className="ContactsList">
      {loading ? <Spinner /> : null}
      {contacts.map((contact, index) => {
        return (
          <Contact
            key={contact.id + index}
            name={contact.name}
            photo={contact.photo}
            email={contact.email}
            phone={contact.phone}
            id={contact.id}
          />
        );
      })}
    </div>
  );
};

export default ContactsList;
