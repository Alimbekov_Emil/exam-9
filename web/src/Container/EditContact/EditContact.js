import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Spinner from "../../Components/UI/Spinner/Spinner";
import { fetchContact, putContact } from "../../store/actions/contactsActions";
import "./EditContact.css";

const EditContact = (props) => {
  const dispatch = useDispatch();

  const [contacts, setContacs] = useState({
    name: "",
    phone: "",
    email: "",
    photo: "",
  });

  const contact = useSelector((state) => state.contacts.contact);
  const loading = useSelector((state) => state.contacts.loading);

  useEffect(() => {
    dispatch(fetchContact(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  useEffect(() => {
    setContacs(contact);
  }, [contact]);

  const contacsDataChanged = (event) => {
    const { name, value } = event.target;

    setContacs((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const editContact = (e) => {
    e.preventDefault();
    dispatch(putContact(props.match.params.id, contacts));
    props.history.push("/");
  };

  return (
    <>
      {loading ? <Spinner /> : null}
      <h2>Edit Contact</h2>
      <form className="FormBlock" onSubmit={editContact}>
        <input
          name="name"
          placeholder="Name"
          className="Input"
          onChange={contacsDataChanged}
          value={contacts.name || ""}
        />
        <input
          name="phone"
          type="number"
          placeholder="Phone"
          className="Input"
          onChange={contacsDataChanged}
          value={contacts.phone || ""}
        />
        <input
          name="email"
          placeholder="Email"
          className="Input"
          onChange={contacsDataChanged}
          value={contacts.email || ""}
        />
        <input
          name="photo"
          placeholder="Photo"
          className="Input"
          onChange={contacsDataChanged}
          value={contacts.photo || ""}
        />
        {contacts.photo !== "" ? (
          <div className="ImgBlock">
            <p>Photo Preview</p>
            <img src={contacts.photo} alt="Contact" />
          </div>
        ) : (
          <div>No Photo</div>
        )}
        <button type="submit" className="FormBtn">
          Save
        </button>
        <NavLink to="/" className="FormBtn">
          Back to contacts
        </NavLink>
      </form>
    </>
  );
};

export default EditContact;
