import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Spinner from "../../Components/UI/Spinner/Spinner";
import { postNewContacts } from "../../store/actions/contactsActions";
import "./AddContacts.css";

const AddContacts = (props) => {
  const dispatch = useDispatch();

  const [contacts, setContacs] = useState({
    name: "",
    phone: "",
    email: "",
    photo: "",
  });

  const loading = useSelector((state) => state.contacts.loading);
  console.log(loading);

  const contacsDataChanged = (event) => {
    const { name, value } = event.target;

    setContacs((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const contacsHandler = (e) => {
    e.preventDefault();
    dispatch(postNewContacts(contacts));

    setContacs({
      name: "",
      phone: "",
      email: "",
      photo: "",
    });
    props.history.push("/");
  };

  return (
    <>
      {loading === true ? <Spinner /> : null}

      <h2>Add New Contacts</h2>
      <form className="FormBlock" onSubmit={contacsHandler}>
        <input
          name="name"
          placeholder="Name"
          className="Input"
          onChange={contacsDataChanged}
          value={contacts.name}
        />
        <input
          name="phone"
          type="number"
          placeholder="Phone"
          className="Input"
          onChange={contacsDataChanged}
          value={contacts.phone}
        />
        <input
          name="email"
          placeholder="Email"
          className="Input"
          type="email"
          onChange={contacsDataChanged}
          value={contacts.email}
        />
        <input
          name="photo"
          placeholder="Photo"
          className="Input"
          onChange={contacsDataChanged}
          value={contacts.photo}
        />
        {contacts.photo !== "" ? (
          <div className="ImgBlock">
            <p>Photo Preview</p>
            <img src={contacts.photo} alt="Contact" />
          </div>
        ) : (
          <div>No Photo</div>
        )}
        <button type="submit" className="FormBtn">
          Save
        </button>
        <NavLink to="/" className="FormBtn">
          Back to contacts
        </NavLink>
      </form>
    </>
  );
};

export default AddContacts;
