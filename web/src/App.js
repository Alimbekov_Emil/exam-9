import { Route, Switch } from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import ContactsList from "./Container/ContactsList/ContactsList";
import "./App.css";
import AddContacts from "./Container/AddContacs/AddContacts";
import EditContact from "./Container/EditContact/EditContact";

const App = () => (
  <Layout>
    <div className="container">
      <Switch>
        <Route path="/" exact component={ContactsList} />
        <Route path="/add-contacts" component={AddContacts} />
        <Route path="/edit-contacts/:id" component={EditContact} />
      </Switch>
    </div>
  </Layout>
);

export default App;
