import axios from "axios";

const axiosContacts = axios.create({
  baseURL: "https://alimbekov-exam-9-default-rtdb.firebaseio.com/",
});

export default axiosContacts;
