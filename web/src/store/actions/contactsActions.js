import axiosContacts from "../../axiosContacts";

export const POST_CONTACTS_REQUEST = "POST_CONTACTS_REQUEST";
export const POST_CONTACTS_SUCCESS = "POST_CONTACTS_SUCCESS";
export const POST_CONTACTS_FAILURE = "POST_CONTACTS_FAILURE";

export const FETCH_CONTACTS_REQUEST = "FETCH_CONTACTS_REQUEST";
export const FETCH_CONTACTS_SUCCESS = "FETCH_CONTACTS_SUCCESS";
export const FETCH_CONTACTS_FAILURE = "FETCH_CONTACTS_FAILURE";

export const FETCH_CONTACT_REQUEST = "FETCH_CONTACT_REQUEST";
export const FETCH_CONTACT_SUCCESS = "FETCH_CONTACT_SUCCESS";
export const FETCH_CONTACT_FAILURE = "FETCH_CONTACT_FAILURE";

export const PUT_CONTACTS_REQUEST = "PUT_CONTACTS_REQUEST";
export const PUT_CONTACTS_SUCCESS = "PUT_CONTACTS_SUCCESS";
export const PUT_CONTACTS_FAILURE = "PUT_CONTACTS_FAILURE";

export const DELETE_CONTACTS_REQUEST = "DELETE_CONTACTS_REQUEST";
export const DELETE_CONTACTS_SUCCESS = "DELETE_CONTACTS_SUCCESS";
export const DELETE_CONTACTS_FAILURE = "DELETE_CONTACTS_FAILURE";

export const postContactsRequest = () => ({ type: POST_CONTACTS_REQUEST });
export const postContactsSuccess = () => ({ type: POST_CONTACTS_SUCCESS });
export const postContactsFailure = (erorr) => ({
  type: POST_CONTACTS_FAILURE,
  erorr,
});

export const fetchContactsRequest = () => ({ type: FETCH_CONTACTS_REQUEST });
export const fetchContactsSuccess = (contacts) => ({
  type: FETCH_CONTACTS_SUCCESS,
  contacts,
});
export const fetchContactsFailure = (erorr) => ({
  type: FETCH_CONTACTS_FAILURE,
  erorr,
});

export const fetchContactRequest = () => ({ type: FETCH_CONTACT_REQUEST });
export const fetchContactSuccess = (contact) => ({
  type: FETCH_CONTACT_SUCCESS,
  contact,
});
export const fetchContactFailure = (erorr) => ({
  type: FETCH_CONTACT_FAILURE,
  erorr,
});

export const putContactsRequest = () => ({ type: PUT_CONTACTS_REQUEST });
export const putContactsSuccess = () => ({ type: PUT_CONTACTS_SUCCESS });
export const putContactsFailure = (erorr) => ({
  type: PUT_CONTACTS_FAILURE,
  erorr,
});

export const deleteContactsRequest = () => ({ type: DELETE_CONTACTS_REQUEST });
export const deleteContactsSuccess = () => ({ type: DELETE_CONTACTS_SUCCESS });
export const deleteContactsFailure = (erorr) => ({
  type: DELETE_CONTACTS_FAILURE,
  erorr,
});

export const postNewContacts = (contacts) => {
  return async (dispatch) => {
    try {
      dispatch(postContactsRequest());
      await axiosContacts.post("contacs.json", contacts);
      dispatch(postContactsSuccess());
      dispatch(fetchContacts());
    } catch (e) {
      dispatch(postContactsFailure(e));
    }
  };
};

export const fetchContacts = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchContactsRequest());
      const response = await axiosContacts.get("contacs.json");
      const contacts = Object.keys(response.data).map((id) => ({
        ...response.data[id],
        id,
      }));
      dispatch(fetchContactsSuccess(contacts));
    } catch (e) {
      dispatch(fetchContactsFailure(e));
    }
  };
};

export const fetchContact = (id) => {
  return async (dispatch) => {
    try {
      dispatch(fetchContactRequest());
      const response = await axiosContacts.get("contacs/" + id + ".json");
      dispatch(fetchContactSuccess(response.data));
    } catch (e) {
      dispatch(fetchContactFailure(e));
    }
  };
};

export const putContact = (id, contact) => {
  return async (dispatch) => {
    try {
      dispatch(putContactsRequest());
      await axiosContacts.put("contacs/" + id + ".json", contact);
      dispatch(putContactsSuccess());
      dispatch(fetchContacts());
    } catch (e) {
      dispatch(putContactsFailure(e));
    }
  };
};

export const deleteContact = (id) => {
  return async (dispatch) => {
    try {
      dispatch(deleteContactsRequest());
      await axiosContacts.delete("contacs/" + id + ".json");
      dispatch(deleteContactsSuccess());
      dispatch(fetchContacts());
    } catch (e) {
      dispatch(deleteContactsFailure(e));
    }
  };
};
