import {
  DELETE_CONTACTS_FAILURE,
  DELETE_CONTACTS_REQUEST,
  DELETE_CONTACTS_SUCCESS,
  FETCH_CONTACTS_FAILURE,
  FETCH_CONTACTS_REQUEST,
  FETCH_CONTACTS_SUCCESS,
  FETCH_CONTACT_FAILURE,
  FETCH_CONTACT_REQUEST,
  FETCH_CONTACT_SUCCESS,
  POST_CONTACTS_FAILURE,
  POST_CONTACTS_REQUEST,
  POST_CONTACTS_SUCCESS,
  PUT_CONTACTS_FAILURE,
  PUT_CONTACTS_REQUEST,
  PUT_CONTACTS_SUCCESS,
} from "../actions/contactsActions";

const initialState = {
  error: false,
  loading: false,
  contacts: [],
  contact: [],
};

const contacsReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_CONTACTS_REQUEST:
      return { ...state, loading: true };
    case POST_CONTACTS_SUCCESS:
      return { ...state, loading: false };
    case POST_CONTACTS_FAILURE:
      return { ...state, loading: false, error: action.error };
    case FETCH_CONTACTS_REQUEST:
      return { ...state, loading: true };
    case FETCH_CONTACTS_SUCCESS:
      return { ...state, loading: false, contacts: action.contacts };
    case FETCH_CONTACTS_FAILURE:
      return { ...state, loading: false, error: action.error };
    case FETCH_CONTACT_REQUEST:
      return { ...state, loading: true };
    case FETCH_CONTACT_SUCCESS:
      return { ...state, loading: false, contact: action.contact };
    case FETCH_CONTACT_FAILURE:
      return { ...state, loading: false, error: action.error };
    case PUT_CONTACTS_REQUEST:
      return { ...state, loading: true };
    case PUT_CONTACTS_SUCCESS:
      return { ...state, loading: false };
    case PUT_CONTACTS_FAILURE:
      return { ...state, loading: false, error: action.error };
    case DELETE_CONTACTS_REQUEST:
      return { ...state, loading: true };
    case DELETE_CONTACTS_SUCCESS:
      return { ...state, loading: false };
    case DELETE_CONTACTS_FAILURE:
      return { ...state, loading: false, error: action.error };

    default:
      return state;
  }
};

export default contacsReducer;
