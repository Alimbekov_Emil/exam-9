import React from "react";
import { NavLink } from "react-router-dom";
import "./Layout.css";

const Layout = (props) => {
  return (
    <>
      <header className="header">
        <div className="container">
          <div className="headerNav">
            <NavLink to="/">Contacts</NavLink>
            <NavLink to="/add-contacts">Add New Contacts</NavLink>
          </div>
        </div>
      </header>
      {props.children}
    </>
  );
};

export default Layout;
