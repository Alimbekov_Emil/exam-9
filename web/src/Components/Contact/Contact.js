import React, { useState } from "react";
import Modal from "../UI/Modal/Modal";
import "./Contact.css";

const Contact = (props) => {
  const [modal, setModal] = useState(false);

  const showInfo = () => {
    setModal(true);
  };
  const hideInfo = () => {
    setModal(false);
  };

  return (
    <>
      <Modal
        id={props.id}
        show={modal}
        closed={hideInfo}
        name={props.name}
        photo={props.photo}
        email={props.email}
        phone={props.phone}
      />
      <div className="Contact" onClick={showInfo}>
        <img src={props.photo} alt="User" />
        <p>{props.name}</p>
      </div>
    </>
  );
};

export default Contact;
